#include <iostream>
#include <iomanip>
using namespace std;
int cetak(int bilangan);
int main()
{
	cout<<"---------------------------------------------"<<endl;
	cout<<setw(30)<<"Konversi Waktu"<<endl;
	cout<<"---------------------------------------------"<<endl;
	int bilangan;
	cout<<"Input bilangan = ";
	cin>>bilangan;
	
	cout<<bilangan<<" detik = ";
	cetak(bilangan);
	return 0;
}
int cetak(int bilangan)
{
	int jam, menit, detik;
	if(bilangan >= 3600)
	{
		 jam = bilangan / 3600;
		 bilangan = bilangan % 3600;
		 menit = bilangan / 60;
		 bilangan = bilangan % 60;
		 detik = bilangan;
	}
	else if( bilangan < 36000 && bilangan >= 60)
	{
		jam = 0;
		menit = bilangan / 60;
		bilangan = bilangan%60;
		detik = bilangan;
	}
	else
	{
		jam = 0;
		menit = 0;
		detik = bilangan;
	}
	cout<<jam<<" Jam "<<menit<<" Menit "<<detik<<" Detik";
	return bilangan;
}
